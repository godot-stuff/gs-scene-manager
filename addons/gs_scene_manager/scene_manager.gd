extends CanvasLayer

@onready var start_bus_volume = AudioServer.get_bus_volume_db(0)


var __animation_volume__: float = 1.0


var animation_volume:
	
	get:
		return __animation_volume__
		
	set(value):
		set_animation_volume(value)


const default_animation_speed = 2.0
const VERSION = "4.x"

var scene_path = ""
var animation_speed = default_animation_speed
var do_volume_animation = false
var mute_bus_volume = -80


func _init():
	
	print(" ")
	print("godot-stuff SceneManager")
	print("https://gitlab.com/godot-stuff/gs-scene-manager")
	print("Copyright 2018-2023, SpockerDotNet LLC. All rights reserved.")
	print("Version " + VERSION)
	print(" ")


func _ready():
	
	$FadeAnimation.animation_finished.connect(on_animation_finished)


func set_animation_volume(value):
	
	if do_volume_animation:
		__animation_volume__ = value
		AudioServer.set_bus_volume_db(0, mute_bus_volume + __animation_volume__*-1*(mute_bus_volume - start_bus_volume))


func transition_to(scene : String , speed : float = 1.0, includeSound : bool = false):
	scene_path = scene
	animation_speed = default_animation_speed * speed
	do_volume_animation = includeSound
	$FadeAnimation.play("fade_out", -1, animation_speed)


func on_animation_finished(animation):
	if animation == "fade_out":
		var results = get_tree().change_scene_to_file(scene_path)
		if results != OK:
			print("error changing scene")
			return
		get_tree().paused = false
		$FadeAnimation.play("fade_in", -1, animation_speed)

