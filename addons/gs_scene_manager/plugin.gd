@tool
extends EditorPlugin


func _enter_tree():
	add_autoload_singleton("SceneManager", "res://addons/gs_scene_manager/scene_manager.tscn")
	
	
func _exit_tree():
	remove_autoload_singleton("SceneManager")
